'use strict'
const mongoose = require('mongoose');
const User = require('../models/user');
const service = require('../services');
const moment = require('moment');
const bitcoin = require('bitcoin');
var config = require('../config');
const amqp = require('amqplib/callback_api');
const request = require('request');
const speakeasy = require('speakeasy');
const _ = require('lodash');
const bcrypt = require('bcrypt-nodejs');
const urlSlug = require('url-slug');
var sendpulse = require("sendpulse-api");



function process_login(string_receiverabit, callback) {

	var build_String = string_receiverabit.split("iqbo");
	var id_user = build_String[0];
	var ip_login = build_String[1];
	var user_agent = build_String[2];

	request({
		url: 'https://freegeoip.net/json/' + ip_login,
		json: true
	}, function (error, response, body) {
		var query = {
			_id: id_user
		};
		var data_update = {
			$push: {
				'security.login_history': {
					'date': Date.now(),
					'ip': body.ip,
					'country_name': body.country_name,
					'user_agent': user_agent
				}
			}
		};
		User.update(query, data_update, function (err, newUser) {
			!err ? (
				//sendmail_login(id_user,body.ip,user_agent,body.country_name,function(cbbs){
				callback(true)
				//})
			) : callback(false)
		});
	})
}

const sendmail_login = function (user_id, ip, user_agent, country_name, callback) {
	User.findOne({ '_id': user_id }, (err, user) => {
		var API_USER_ID = config.API_USER_ID;
		var API_SECRET = config.API_SECRET;
		var TOKEN_STORAGE = "/tmp/"
		sendpulse.init(API_USER_ID, API_SECRET, TOKEN_STORAGE);
		const answerGetter = function answerGetter(data) {
			console.log(data);
		}

		callback(true);
	})
}

function process_login_rabit(string_receiverabit, callback) {

	process_login(string_receiverabit, function (cb) {
		cb ? callback(true) : callback(false)
	});
}

module.exports = {
	process_login_rabit
}