#!/bin/bash

LOADING=false
DEBUG=/dev/null

usage()
{
    cat << EOF
    usage: $0 [options] <DBNAME>

    OPTIONS:
        -h      Show this help.
        -l      Load instead of export
        -u      Mongo username
        -p      Mongo password
        -H      Mongo host string (ex. localhost:27017)
        -q      Mongo query for export (JSON string)
        -L      Limit exported results to this amount
        -d      Show (debug) error messages in console
EOF
}

while getopts "hldu:p:H:q:L:" opt; do
    MAXOPTIND=$OPTIND

    case $opt in
        h)
            usage
            exit
            ;;
        l)
            LOADING=true
            ;;
        d)
            DEBUG=/dev/stderr
            ;;
        u)
            USERNAME="$OPTARG"
            ;;
        p)
            PASSWORD="$OPTARG"
            ;;
        H)
            HOST="$OPTARG"
            ;;
        q)
            QUERY="$OPTARG"
            ;;
        L)
            LIMIT="$OPTARG"
            ;;
        \?)
            echo "Invalid option $opt"
            exit 1
            ;;
    esac
done

shift $(($MAXOPTIND-1))

if [ -z "$1" ]; then
    echo "Usage: $0 [options] <DBNAME>"
    exit 1
fi

DB="$1"
if [ -z "$HOST" ]; then
    HOST="localhost:27017"
    CONN="localhost:27017/$DB"
else
    CONN="$HOST/$DB"
fi

ARGS=""
if [ -n "$USERNAME" ]; then
    ARGS="-u $USERNAME"
fi
if [ -n "$PASSWORD" ]; then
    ARGS="$ARGS -p $PASSWORD"
fi

if $LOADING ; then
    echo "*************************** Mongo Import ************************"
    echo "**** Host:      $HOST"
    echo "**** Database:  $DB"
    echo "**** Username:  $USERNAME"
    echo "**** Password:  $PASSWORD"
    echo "*****************************************************************"

    if [ ! -f $DB.tar.gz ]; then
        echo "Archive $DB.tar.gz to import from could not be found! Aborting ..."
        exit
    fi

    tar -xzf $DB.tar.gz 2>$DEBUG
    pushd $DB 2>$DEBUG

    for path in *.json; do
        collection=${path%.json}
        echo ""
        echo "___________________ $collection _____________________"
        echo "Loading into $DB/$collection from $path"
        mongoimport --host $HOST $ARGS -d $DB -c $collection --file $path
        echo "_____________________________________________________"
        echo ""
    done

    popd 2>$DEBUG
    rm -rf $DB 2>$DEBUG
else
    echo "*************************** Mongo Export ************************"
    echo "**** Host:      $HOST"
    echo "**** Database:  $DB"
    echo "**** Username:  $USERNAME"
    echo "**** Password:  $PASSWORD"
    echo "**** Query:     $QUERY"
    echo "**** Limit:     $LIMIT"
    echo "*****************************************************************"

    DATABASE_COLLECTIONS=$(mongo $CONN $ARGS --quiet --eval 'db.getCollectionNames().join()' | sed 's/,/ /g')

    mkdir -p tmp/$DB 2>$DEBUG
    pushd tmp/$DB  2>$DEBUG

    if [ -n "$QUERY" ]; then
        ARGS="$ARGS -q $QUERY"
    fi

    for collection in $DATABASE_COLLECTIONS; do
        echo "Exporting collection $collection ..."
        mongoexport --host $HOST $ARGS -d $DB -c $collection -o $collection.json 2>$DEBUG
        if [ "$LIMIT" != "" ]; then
            head -n $LIMIT $collection.json > $collection.json.tmp && mv $collection.json.tmp $collection.json
            echo "limited result set to $LIMIT records"
        fi
        # TODO: For 2.6, mongoexport supports a limit argument (https://jira.mongodb.org/browse/SERVER-2033).
        #       Support this native way here as default. Reintroduce the --jsonArray option in that case, probably.
    done

    pushd .. 2>$DEBUG
    tar -czf "$DB.tar.gz" $DB 2>$DEBUG
    popd 2>$DEBUG
    popd 2>$DEBUG
    mv tmp/$DB.tar.gz ./ 2>$DEBUG
    rm -rf tmp 2>$DEBUG
fi
