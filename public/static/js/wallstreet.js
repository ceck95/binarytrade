'use strict';
$(document).ready(function() {
    $('.btnfrmWallStreet').on('click',function(){
        var amount = $('#amount_wallstreet').val();
        if (parseFloat(amount) > 0)
        {
            $.ajax({
                url: "/account/wall-street/submit",
                data: {
                    'amount' : amount
                },
                type: "POST",
                beforeSend: function(result) {

                    $('.btnfrmWallStreet').button('loading');
                    
                },
                error: function(data) {
                    $('.btnfrmWallStreet').button('reset');
                    $('#frmWallStreet .alert').show().html(data.responseJSON.message);
                },
                success: function(data) {
                    $('#frmWallStreet .alert').show().html('Subscribe success') ;
                    $('.btnfrmWallStreet').button('reset');
                    setTimeout(function() {
                        location.reload(true);
                    }, 1000);
                }
            });   
        }
    });

    $('.btnfrmWallStreetWithdraw').on('click',function(){
        var amount = $('#amount_wallstreet_widthdraw').val();
        if (parseFloat(amount) > 0)
        {
            $.ajax({
                url: "/account/wall-street/submit-widthdraw",
                data: {
                    'amount' : amount
                },
                type: "POST",
                beforeSend: function(result) {

                    $('.btnfrmWallStreetWithdraw').button('loading');
                    
                },
                error: function(data) {
                    $('.btnfrmWallStreetWithdraw').button('reset');
                    $('#frmWallStreetWithdraw .alert').show().html(data.responseJSON.message);
                },
                success: function(data) {
                    $('#frmWallStreetWithdraw .alert').show().html('Fund Out success') ;
                    $('.btnfrmWallStreetWithdraw').button('reset');
                    setTimeout(function() {
                        location.reload(true);
                    }, 1000);
                }
            });   
        }
    });
})
