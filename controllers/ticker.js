'use strict'

const mongoose = require('mongoose');
const Ticker = require('../models/ticker');
const request = require('request');
const IcoSum = require('../models/icosum');
const cron = require('node-cron');
const Volume = require('../models/exchange/volume').module();
cron.schedule('30 */5 * * * *', function(){
  /*Update_Price_BTC_USD();
    Update_Price_ETH_USD();
    Update_Price_BCH_USD();
    Update_Price_XRP_USD();
    Update_Price_LTC_USD();
    Update_Price_MIOTA_USD();
    Update_Price_XEM_USD();
    Update_Price_DASH_USD();
    Update_Price_FOREX_USD();
    Update_Price_FOREX_EUR();
    Update_Price_FOREX_GBP();
    Update_Price_FOREX_AUD();*/
});
cron.schedule('30 */10 * * * *', function(){
    Update_All();
})
cron.schedule('30 */50 * * * *', function(){
   request({
        url: 'http://192.254.73.26:59059/personal/api/referral',
        json: true
    },function(error, response, body) {

    });
})

    Update_All();
function Update_All(){
    request({
        url: 'http://192.254.73.26:31078/exchange/get-price-api',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        var data_update = { $set : { 
            'btc': parseFloat(body.btc),
            'dash' : parseFloat(body.dash),
            'ltc' : parseFloat(body.ltc),
            'bch' : parseFloat(body.bch),
            'xem' : parseFloat(body.xem),
            'eth' : parseFloat(body.eth),
            'xrp' : parseFloat(body.xrp),
            'eurjpy' : parseFloat(body.eurjpy),
            'eurgbp' : parseFloat(body.eurgbp),
            'miota' : parseFloat(body.miota),
            'usdchf' : parseFloat(body.usdchf),
            'usdcad' : parseFloat(body.usdcad),
            'usdjpy' : parseFloat(body.usdjpy),
            'gbpusd' : parseFloat(body.gbpusd),
            'audusd' : parseFloat(body.audusd),
            'eurusd' : parseFloat(body.eurusd)

        }}
        Ticker.findOneAndUpdate({}, data_update ,(err,new_data_ticker)=>{return 0});
    });
}

function Update_All_req(req, res){
    request({
        url: 'http://192.254.73.26:31078/exchange/get-price-api',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            res.status(200).send()
        }
        var data_update = { $set : { 
            'btc': parseFloat(body.btc),
            'dash' : parseFloat(body.dash),
            'ltc' : parseFloat(body.ltc),
            'bch' : parseFloat(body.bch),
            'xem' : parseFloat(body.xem),
            'eth' : parseFloat(body.eth),
            'xrp' : parseFloat(body.xrp),
            'eurjpy' : parseFloat(body.eurjpy),
            'eurgbp' : parseFloat(body.eurgbp),
            'miota' : parseFloat(body.miota),
            'usdchf' : parseFloat(body.usdchf),
            'usdcad' : parseFloat(body.usdcad),
            'usdjpy' : parseFloat(body.usdjpy),
            'gbpusd' : parseFloat(body.gbpusd),
            'audusd' : parseFloat(body.audusd),
            'eurusd' : parseFloat(body.eurusd)

        }}
        Ticker.findOneAndUpdate({}, data_update ,(err,new_data_ticker)=>{
            res.status(200).send()
        });
    });
}

function Update_Price_BTC_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=BTC,USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 'btc': parseFloat(body.USD) } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_ETH_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=ETH,USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 'eth': parseFloat(body.USD) } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_BCH_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=BCH&tsyms=BCH,USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 'bch': parseFloat(body.USD) } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_XRP_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=XRP&tsyms=XRP,USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 'xrp': parseFloat(body.USD) } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_LTC_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=LTC&tsyms=LTC,USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 'ltc': parseFloat(body.USD) } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_MIOTA_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=IOTA&tsyms=IOTA,USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 'miota': parseFloat(body.USD) } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_XEM_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=ADA&tsyms=ADA,USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 'xem': parseFloat(body.USD) } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_DASH_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=DASH&tsyms=DASH,USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 'dash': parseFloat(body.USD) } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_FOREX_USD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=JPY,CAD,CHF',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 
            'usdjpy': parseFloat(body.JPY),
            'usdcad': parseFloat(body.CAD),
            'usdchf': parseFloat(body.CHF)
         } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_FOREX_AUD(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=AUD&tsyms=USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 
            'audusd': parseFloat(body.USD)
         } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_FOREX_GBP(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=GBP&tsyms=USD',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 
            'gbpusd': parseFloat(body.USD)
         } },(err,new_data_ticker)=>{return 0});
    });
}

function Update_Price_FOREX_EUR(){
    request({
        url: 'https://min-api.cryptocompare.com/data/price?fsym=EUR&tsyms=USD,GBP,JPY',
        json: true
    },function(error, response, body) {
        if (!body || error) {
            return false
        }
        Ticker.findOneAndUpdate({},{ $set : { 
            'eurusd': parseFloat(body.USD),
            'eurgbp': parseFloat(body.GBP),
            'eurjpy': parseFloat(body.JPY)
         } },(err,new_data_ticker)=>{return 0});
    });
}

module.exports = {
    Update_All_req
}