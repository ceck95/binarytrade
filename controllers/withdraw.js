'use strict'

const mongoose = require('mongoose');
const User = require('../models/user');
const Withdraw = require('../models/withdraw');
const service = require('../services');
const moment = require('moment');
const Ticker = require('../models/ticker');
const _ = require('lodash');
const bcrypt = require('bcrypt-nodejs');
const nodemailer = require('nodemailer');
const bitcoin = require('bitcoin');

var sendpulse = require("sendpulse-api");
var sendpulse = require("../models/sendpulse.js");

var TOKEN_STORAGE = "/tmp/"
var config = require('../config');
var API_SECRET = config.API_SECRET;
var API_USER_ID = config.API_USER_ID;
sendpulse.init(API_USER_ID, API_SECRET, TOKEN_STORAGE);

const STCclient = new bitcoin.Client({
	host: config.BBL.host,
	port: config.BBL.port,
	user: config.BBL.user,
	pass: config.BBL.pass,
	timeout: config.BBL.timeout
});

const BTCclient = new bitcoin.Client({
	host: config.BTC.host,
	port: config.BTC.port,
	user: config.BTC.user,
	pass: config.BTC.pass,
	timeout: config.BTC.timeout
});




function Index(req, res) {
	res.locals.title = 'Withdraw'
	res.locals.menu = 'withdrawal'
	res.locals.user = req.user
	res.render('account/withdraw');
}
function WithdrawSubmit(req, res) {
	var amountUsd = parseFloat(req.body.amount);
	console.log(req.body);
	if (!amountUsd)
		return res.status(404).send({ error: 'amount', message: 'Ensure wallet has sufficient balance' });

	var type = req.body.type;
	var wallet = req.body.wallet;

	var user = req.user;
	let newWithdraw = new Withdraw();

	!user.validPassword(_.trim(req.body.password))
	if (!user.validPassword(_.trim(req.body.password)))
		return res.status(404).send({ error: 'password', message: 'Wrong Password!' });

	if (type == 'btc' || type == 'coin' && wallet != '' && !isNaN(amountUsd)) {

		// type = Coin
		var ast_balance = parseFloat(user.balance.coin_wallet.available);

		if (parseFloat(ast_balance) < parseFloat(amountUsd) || parseFloat(amountUsd) <= 5) {
			return res.status(404).send({ error: 'amount', message: 'Ensure wallet has sufficient balance!' });
		} else {

			STCclient.validateAddress(wallet, function (err, valid) {
				if (err) {
					return res.status(404).send('Error Validate Address!');
				} else {
					if (valid.isvalid) {
						Ticker.findOne({}, (err, data_ticker) => {
							if (err) {
								return res.status(404).send({ error: 'amount', message: 'Ensure wallet has sufficient balance!' });
							} else {
								var ast_usd = parseFloat(data_ticker.price_usd);

								var ast_withdraw_usd = (parseFloat(amountUsd) * parseFloat(ast_usd)).toFixed(8);
								ast_withdraw_usd = parseFloat(ast_withdraw_usd);
								console.log(ast_balance);
								console.log(amountUsd);
								var new_ast_balance = parseFloat(ast_balance - amountUsd).toFixed(8);
								var token_withdraw = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null), '?', '_');
								var today = moment();
								newWithdraw.active_email = 0;
								newWithdraw.amount = amountUsd;
								newWithdraw.amount_usd = parseFloat(ast_withdraw_usd);
								newWithdraw.user_id = user._id;
								newWithdraw.status = 'pending';
								newWithdraw.username = user.displayName;
								newWithdraw.wallet = wallet;
								newWithdraw.txid = '';
								newWithdraw.fee = '0';
								newWithdraw.date = moment(today).format();
								newWithdraw.type = 'SFCC';
								newWithdraw.token_withdraw = token_withdraw;
								newWithdraw.rate = ast_usd;
								newWithdraw.save((err, WithdrawStored) => {
									if (err) {
										res.status(500).send({ message: `Error Withdraw` })
									} else {
										var query = { _id: user._id };
										var data_update = {
											$set: {
												'balance.coin_wallet.available': parseFloat(new_ast_balance)
											},
											$push: {
												'balance.coin_wallet.history': {
													date: Date.now(),
													type: 'sent',
													amount: amountUsd,
													detail: 'Withdraw ' + parseFloat(amountUsd) + ' SFCC ($ ' + ast_withdraw_usd + ') from SFCC wallet <br> Exchange rate: 1 SFCC = ' + parseFloat(ast_usd) + ' USD'
												}
											}
										};
										User.update(query, data_update, function (err, UsersUpdate) {
											if (err) res.status(500).send({ message: `Error` })

											mailConfirmWithdraw(token_withdraw, amountUsd, wallet, user, WithdrawStored, function (data) {

												return res.status(200).send({ error: '', status: 1, message: 'Withdraw success' });
											});

										});
									}

								})

							}



						});
					} else {
						return res.status(404).send({ error: 'wallet', message: 'Please enter a valid address!' });
					}
				}

			});
		}

	} else {
		res.status(403).send('Forbidden')
	}

}
// function WithdrawSubmit(req, res){
// 	var amountUsd = parseFloat(req.body.amount);

// 	if(!amountUsd)
// 		return res.status(404).send({error: 'amount', message: 'Ensure wallet has sufficient balance'});

// 	var type = req.body.type;
// 	var wallet = req.body.wallet;

// 	var user = req.user;
// 	let newWithdraw = new Withdraw();




// 	if (type == 'btc' || type == 'coin' && wallet != '' && !isNaN(amountUsd)) {

// 			// type = Coin
// 			var ast_balance = parseFloat(user.balance.coin_wallet.available);

// 			if (parseFloat(ast_balance) < parseFloat(amountUsd) || parseFloat(amountUsd) <= 0) {
// 				return res.status(404).send({error: 'amount', message: 'Ensure wallet has sufficient balance!'});
// 			}else{

// 				STCclient.validateAddress(wallet, function (err, valid) {
// 					if(err){
// 						return res.status(404).send('Error Validate Address!');
// 					}else{
// 						if (valid.isvalid) {
// 							Ticker.findOne({},(err,data_ticker)=>{
// 								if (err) {
// 									return res.status(404).send({error: 'amount', message: 'Ensure wallet has sufficient balance!'});
// 								}else{
// 									var ast_usd = parseFloat(data_ticker.price_usd);

// 									var ast_withdraw_usd = (parseFloat(amountUsd)*parseFloat(ast_usd)).toFixed(8);
// 										ast_withdraw_usd = parseFloat(ast_withdraw_usd);
// 										console.log(ast_balance);
// 										console.log(amountUsd);
// 									var new_ast_balance = parseFloat(ast_balance - amountUsd).toFixed(8);
// 									var token_withdraw = _.replace(bcrypt.hashSync(new Date(), bcrypt.genSaltSync(8), null),'?','_');
// 									var withdraw_uid = (new Date().getTime()).toString(36);
// 									var data_update = {
// 										$push: {
// 											'withdraw': {
// 												"withdraw_id": withdraw_uid,
// 												"active_email" : 0,
// 												"amount" : amountUsd,
// 												"amount_usd" : parseFloat(ast_withdraw_usd),
// 												"user_id" : user._id,
// 												"status" : 'pending',
// 												"username": user.displayName,
// 												"wallet": wallet,
// 												"txid": '',
// 												"fee": 0,
// 												"date": Date.now(),
// 												"type": 'LEC',
// 												"token_withdraw": token_withdraw
// 											}
// 										}
// 									};
// 									User.update({_id:user._id}, data_update, function(err, UsersUpdate){
// 										if(err) res.status(500).send({message: `Error`})
// 										var query = {_id:user._id};
// 										var data_update = {
// 											$set : {
// 												'balance.coin_wallet.available': parseFloat(new_ast_balance)
// 											},
// 											$push: {
// 												'balance.coin_wallet.history': {
// 													date: Date.now(), 
// 													type: 'sent', 
// 													amount: amountUsd, 
// 													detail: 'Withdraw ' +parseFloat(amountUsd) + ' STC ($ '+ast_withdraw_usd+') <br> Exchange rate: 1 STC = '+parseFloat(ast_usd)+' USD'
// 												}
// 											}
// 										};
// 										User.update(query, data_update, function(err, UsersUpdate){
// 											if(err) res.status(500).send({message: `Error`})

// 											mailConfirmWithdraw(token_withdraw, amountUsd, wallet, user, function(data){

// 												return res.status(200).send({error: '', status: 1, message: 'Withdraw success'});
// 											});

// 										});

// 									});
// 								}



// 							});
// 						}else{
// 							return res.status(404).send({error: 'wallet', message: 'Please enter a valid address!'});
// 						}
// 					}


// 				});
// 			}




// 	}else{
// 		res.status(403).send('Forbidden')
// 	}

// }
function LoadDataWithdraw(req, res) {
	Withdraw.find({ user_id: req.session.userId }, (err, data) => {

		if (err) return res.status(500).send({ message: `Error load your withdraw` })
		let data_withdraw = data;


		var new_data_withdraw = [];
		if (data_withdraw == undefined || _.size(data_withdraw) === 0)
			return res.status(404).send({ message: 'No data' });

		_.forEach(data_withdraw, function (value) {
			new_data_withdraw.push({
				'date': moment(value.date).format('MM/DD/YYYY LT'),
				'amount': value.amount,
				'type': value.type,
				'status': (value.status == 'pending' ? 'Pending' : 'Completed'),
				'wallet': value.wallet,
				'txid': value.txid
			});
		});
		return res.status(200).send({ withdraw: new_data_withdraw });

	})
}
// function LoadDataWithdraw(req, res){
// 	User.findOne({_id: req.session.userId}, { "withdraw": 1 },(err,data)=>{

// 		if(err) return res.status(500).send({message:`Error load your withdraw`})
// 		let data_withdraw = data.withdraw;


// 		var new_data_withdraw = [];
// 		if (data_withdraw == undefined || _.size(data_withdraw) === 0)
// 			return res.status(404).send({message: 'No data'});

// 		_.forEach(data_withdraw, function(value) {
// 			new_data_withdraw.push({
// 				'date': moment(value.date).format('MM/DD/YYYY LT'),
// 				'amount': value.amount,
// 				'type': value.type, 
// 				'status': (value.status == 'pending' ? 'Pending' : 'Completed'),
// 				'wallet': value.wallet,
// 				'txid': value.txid
// 			});
// 		});
// 		return res.status(200).send({withdraw: new_data_withdraw});

// 	})
// }
function mailConfirmWithdraw(token_withdraw, amount_ast, address_stc, user, withdrawStored, callback) {



}
function active(req, res) {
	let token = null;
	_.has(req.query, 'token') ? (
		token = _.split(req.query.token, '_'),
		token.length > 1 && (
			Withdraw.findOneAndUpdate({
				'_id': token[2],
				'user_id': token[1],
				'token_withdraw': token[0],
				'active_email': 0
			}, {
					'active_email': 1,
					'status': 'finish'
				}, function (err, result) {
					if (result) {
						if (result.type == 'BCH') {
							var wallet = result.wallet;
							var amount = result.amount;
							//send form
							/*BCHclient.sendFrom("account_admin",wallet,parseFloat(amount), function (err, result) {
															if (err){
																	return res.status(500).send({message: "Transaction failed, Please try again"});
															}
															else
															{
																return res.status(200).send({message: 'Successful transaction'});
															}
													});*/
						}

						if (result.type == 'BTC') {
							var wallet = result.wallet;
							var amount = result.amount;
							//send form
							/*BTCclient.sendFrom("account_admin",wallet,parseFloat(amount), function (err, result) {
															if (err){
																	return res.status(500).send({message: "Transaction failed, Please try again"});
															}
															else
															{
																return res.status(200).send({message: 'Successful transaction'});
															}
													});*/
						}


						if (result.type == 'BBL') {
							var wallet = result.wallet;
							var amount = result.amount;
							//send form
							/*STCclient.sendFrom("account_admin",wallet,parseFloat(amount), function (err, result) {
															if (err){
																	return res.status(500).send({message: "Transaction failed, Please try again"});
															}
															else
															{
																return res.status(200).send({message: 'Successful transaction'});
															}
													});*/
						}


						res.redirect('/login-system.html')
					}
					else {
						res.redirect('/login-system.html');
					}
				})
		)
	) : (
			res.redirect('/login-system.html')
		)
}
module.exports = {
	Index,
	WithdrawSubmit,
	LoadDataWithdraw,
	active
}