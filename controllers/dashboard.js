'use strict'

const mongoose = require('mongoose');
const User = require('../models/user');
const service = require('../services');
const Ticker = require('../models/ticker');
const Order = require('../models/order');
const History = require('../models/history');
const Partner = require('../models/partner');
const HistoryCommission = require('../models/history_commision');
function IndexOn(req,res){
	// setupTicker();
	User.find({'$and' : [{'p_node' : req.user._id},{'betting': {$gte: 20000000}}]},function(ess,check_f1){
		HistoryCommission.find({'user_id' : req.user._id},function(errs,datass){
			if (!errs && datass)
			{
				var amount_commission_1 = 0,amount_commission_2 = 0,amount_commission_3 = 0,amount_commission_4 = 0;
				if (parseInt(req.user.level) == 1)
				{
					amount_commission_1 = parseFloat(req.user.deposit_node) * 1.5 /100;
				}
				else if (parseInt(req.user.level) == 2)
				{
					amount_commission_2 = parseFloat(req.user.deposit_node) * 1 /100;
				}
				else if (parseInt(req.user.level) == 3)
				{
					amount_commission_3 = parseFloat(req.user.deposit_node) * 0.5 /100;
				}
				else if (parseInt(req.user.level) == 4)
				{
					amount_commission_4 = parseFloat(req.user.deposit_node) * 2 /100;
				}
					res.locals.title = 'Dashboard';
					res.locals.menu = 'dashboard';
					res.locals.user = req.user;
					res.locals.count_child = check_f1.length;
					res.locals.amount_commission_1 = amount_commission_1;
					res.locals.amount_commission_2 = amount_commission_2;
					res.locals.amount_commission_3 = amount_commission_3;
					res.locals.amount_commission_4 = amount_commission_4;
					res.locals.commision_history = datass;
					
					res.render('account/dashboard');
			}
		})
	})
	
}

function get_aff_share_ib(req, res){
	var amount_ib_f1 = 0;
	var amount_ib_f2 = 0;
	var amount_ib_f3 = 0;
	User.find({'ib_share_f1': {$gte: 5}},function(errs_f1,datas_f1){
		var count_ib_share_f1 = datas_f1.length;
		User.find({'ib_share_f2': {$gte: 25}},function(errs_f2,datas_f2){
			var count_ib_share_f2 = datas_f2.length;
			User.findOne({'_id' : '5aefc080e5c30f38711870e0'},function(errs,result){
				if (!errs && result)
				{
					amount_ib_f1 = ((parseFloat(result.balance_bk) * 0.03)/100000000).toFixed(8);
					amount_ib_f2 = ((parseFloat(result.balance_bk) * 0.02)/100000000).toFixed(8);
					amount_ib_f3 = ((parseFloat(result.balance_bk) * 0.05)/100000000).toFixed(8);
					return res.status(200).send({
						'a1': count_ib_share_f1, 
						'a2': count_ib_share_f2,
						'a3' : 0,
						'b1' : amount_ib_f1,
						'b2' : amount_ib_f2,
						'b3' : amount_ib_f3,
					}) 
				}
			})
		})
	})
}

function setupTicker(){
	let newTicker = new Ticker();
	newTicker.last= '0.5';
	newTicker.bid= '0.5';
	newTicker.ask= '0.5';
	newTicker.high= '0.5';
	newTicker.volume= '0.5';
	newTicker.price_usd= '0.5';
	newTicker.price_btc= '0.5';
	newTicker.save((err, investStored)=>{
		console.log(investStored);
	});
}


function TransferToCoin(req, res){
	var amountUsd = parseFloat(req.body.amount);
	
	var user = req.user;
	var balance_lending = parseFloat(user.balance.lending_wallet.available).toFixed(8);
	var balance_coin = parseFloat(user.balance.coin_wallet.available).toFixed(8);
	
	
	if ( amountUsd < 5 || amountUsd > balance_lending || isNaN(amountUsd))
		return res.status(404).send({message: 'Please enter amount > 5$!'})

	Ticker.findOne({},(err,data_ticker)=>{
		if(err){
			res.status(500).send({message: `Error al crear el usuario: ${err}`})
		}else{
			amountUsd = parseFloat(amountUsd).toFixed(8);
			var ast_usd = data_ticker.price_usd;
			var amount = parseFloat(amountUsd)/ parseFloat(ast_usd);
			amount = parseFloat(amount).toFixed(8);
			var query = {_id:user._id};
			var new_balance_lending = parseFloat(balance_lending) - parseFloat(amountUsd);
			new_balance_lending = parseFloat(new_balance_lending).toFixed(8);
			var new_balance_coin = parseFloat(balance_coin) + parseFloat(amount);
			new_balance_coin = parseFloat(new_balance_coin).toFixed(8);
			var data_update = {
				$set : {
					'balance.lending_wallet.available': parseFloat(new_balance_lending),
					'balance.coin_wallet.available': parseFloat(new_balance_coin)
				},
				$push: {
					'balance.lending_wallet.history': {
						date: Date.now(), 
						type: 'sent', 
						amount: parseFloat(amountUsd), 
						detail: 'Transfer to BBL wallet  $' +parseFloat(amountUsd) + ' ('+ parseFloat(amount)+' BBL) <br> Exchange rate: 1 BBL = '+parseFloat(ast_usd)+' USD'
					},
					'balance.coin_wallet.history': {
						date: Date.now(), 
						type: 'received', 
						amount: parseFloat(amount), 
						detail: 'Received from USD wallet $' +parseFloat(amountUsd) + ' ('+ parseFloat(amount)+' BBL) <br> Exchange rate: 1 BBL = '+parseFloat(ast_usd)+' USD'
					}
				}
			};

			User.update(query, data_update, function(err, Users){
				if(err) res.status(500).send({message: `Error al crear el usuario: ${err}`})
				return res.status(200).send({
					message: 'Transfer success', 
					balance_lending: parseFloat(new_balance_lending),
					balance_coin: parseFloat(new_balance_coin)
				}) /*service son como helpers*/
			});
		} 
		

	});

	
}
/*function History_tempalte(req,res){
	
	History.find({'user_id' : req.partner.account_id},(err,result)=>{
		
		res.locals.title = 'Exchange',
		res.locals.result = result,
		res.locals.menu = 'exchange',
		res.locals.user = req.user;
		res.locals.AccountID = req.params.AccountID,
		res.locals.partner = req.partner,
		res.render('account/historybuysell')
	});
}*/

function History_tempalte(req,res){
	
	History.find({'$and' : [{'user_id' : req.partner.account_id},{'$or' : [{'type' : 'win'},{'type' : 'lose'}]}]} ,(err,result)=>{
		History.find({'$and' : [{'user_id' : req.partner.account_id},{'$or' : [{'type' : 'deposit'},{'type' : 'withdraw'}]}]} ,(errs,results)=>{
			res.locals.title = 'History '+req.partner.account_id,
			res.locals.result = result,
			res.locals.results = results,
			res.locals.menu = 'exchange',
			res.locals.user = req.user;
			res.locals.AccountID = req.params.AccountID,
			res.locals.partner = req.partner,
			res.locals.layout = 'layout_exchange.hbs',
			res.render('account/historybuysell')
		}).sort( { $date: 1 } )
	}).sort( { $date: 1 } )
}

module.exports = {
	IndexOn,
	TransferToCoin,
	History_tempalte,
	get_aff_share_ib
}