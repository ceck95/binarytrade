'use strict'

const User = require('../../models/user');
const Withdraw = require('../../models/withdraw');
const Ticker = require('../../models/ticker');
const Invest = require('../../models/invest');
const IcoSum = require('../../models/icosum');
const Ico = require('../../models/ico');
const moment = require('moment');
const speakeasy = require('speakeasy');
const _ = require('lodash');
const Order = require('../../models/order');
const nodemailer = require('nodemailer');
var sendpulse = require("sendpulse-api");
function ListIco(req, res){
	IcoSum.findOne({},(err,total_ico)=>{
		Order.find({status: '0'}, (err, data)=>{
			if (err) {
				res.status(500).send({'message': 'data not found'});
			}else{
				// res.status(200).send(users);
				res.render('admin/ico', {
					title: 'Ico',
					layout: 'layout_admin.hbs',
					ico: data,
					total_ico : total_ico
				});
			}
		})
	})
}

function ListIcohistory(req, res){
	Order.find({}, (err, data)=>{
		if (err) {
			res.status(500).send({'message': 'data not found'});
		}else{
			// res.status(200).send(users);
			res.render('admin/ico_history', {
				title: 'Ico',
				layout: 'layout_admin.hbs',
				ico: data
			});
		}
	})
}

function FindOrder(order_id,callback){
	Order.findById(order_id, (err, data) => {
		err || !data ? callback(false) : callback(data);
	 });
}
var update_balace = function(name , new_ast_balance,user_id,callback){
	var obj = null;
	if (name === 'BTC') obj =  { 'balance.bitcoin_wallet.available': parseFloat(new_ast_balance) }
	if (name === 'BCH') obj =  {'balance.bitcoincash_wallet.available' : parseFloat(new_ast_balance)};
	if (name === 'BCC') obj = {'balance.bitconnect_wallet.available' : parseFloat(new_ast_balance)};
	if (name === 'LTC') obj = {'balance.litecoin_wallet.available': parseFloat(new_ast_balance)};
	if (name === 'ADA') obj = {'balance.coin_wallet.available': parseFloat(new_ast_balance)};
	if (name === 'DASH') obj = {'balance.dashcoin_wallet.available': parseFloat(new_ast_balance)};
	if (name === 'XVG') obj = {'balance.verge_wallet.available': parseFloat(new_ast_balance)};
	if (name === 'BTG') obj = {'balance.bitcoingold_wallet.available': parseFloat(new_ast_balance)};
	if (name === 'XZC') obj = {'balance.zcoin_wallet.available': parseFloat(new_ast_balance)};
	if (name === 'ETH') obj = {'balance.ethereum_wallet.available': parseFloat(new_ast_balance)};
	User.update({ _id :user_id }, { $set : obj }, function(err, UsersUpdate){
		err ? callback(false) : callback(true);
	});
}
var get_balance =function(name,user_id,callback){
	var balance = 0;
	User.findOne({'_id' : user_id},(err,data)=>{
		(!err && data)? (
			name === 'BTC' && callback(data.balance.bitcoin_wallet.available),
			name === 'BCH' && callback(data.balance.bitcoincash_wallet.available),
			name === 'BCC' && callback(data.balance.bitconnect_wallet.available),
			name === 'LTC' && callback(data.balance.litecoin_wallet.available),
			name === 'ADA' && callback(data.balance.coin_wallet.available),
			name === 'DASH' && callback(data.balance.dashcoin_wallet.available),
			name === 'XVG' && callback(data.balance.verge_wallet.available),
			name === 'BTG' && callback(data.balance.bitcoingold_wallet.available),
			name === 'XZC' && callback(data.balance.zcoin_wallet.available),
			name === 'ETH' && callback(data.balance.ethereum_wallet.available)
		) : callback (balance) 
	})
}
function CanelICO(req, res){
	var query_update;
	var data_update;
	FindOrder(req.params.id,function(data){
		data && data.status == 0 ? (
			query_update = {_id:data._id},
			data_update = {$set : {'status': 3}},
			Order.update(query_update,data_update,function(err, newUser){
				get_balance(data.method_payment,data.user_id,function(balance_user){
					var new_ast_balance = parseFloat(balance_user) + parseFloat(data.amount_payment);
					update_balace(data.method_payment,new_ast_balance,data.user_id,function(callback){
						sendmail_cancel_ico(data.user_id,function(cbb){
							res.redirect('/qweqwe/admin/ico#success')
						})
					})
				});
	        })
		): res.redirect('/qweqwe/admin/ico#error')
	})
}

function EndICO(req, res){
	IcoSum.update({},{$set : {'status' : 0}},(err,result_order)=>{
		res.redirect('/qweqwe/admin/ico');
	}); 
}
function StartICO(req, res){
	IcoSum.update({},{$set : {'status' : 1}},(err,result_order)=>{
		res.redirect('/qweqwe/admin/ico');
	}); 
}
function TotalBuy(req, res){
	IcoSum.update({},{$set : {'total' :parseFloat(req.body.total)}},(err,result_order)=>{
		res.redirect('/qweqwe/admin/ico');
	}); 
}

var getUser = function(id_user,callback){
	User.findById(id_user, function(err, user) {
		err || !user ? callback(null) : callback(user);
	});
}
var update_balace_ADA = function(new_ast_balance,user_id,callback){
	var query = {_id:user_id};
	var data_update = {
		$set : {
			'balance.coin_wallet.available': parseFloat(new_ast_balance)
		}
	};
	User.update(query, data_update, function(err, UsersUpdate){
		err ? callback(false) : callback(true);
	});
}
var update_balance_ico_add = function(user_id,amount,callback){
	getUser(user_id,function(user){
		if (user) 
		{
			var ast_balance = parseFloat(user.balance.coin_wallet.available);
			var new_ast_balance = parseFloat(ast_balance + amount).toFixed(8);
			update_balace_ADA(new_ast_balance,user._id,function(calb){
				calb ? callback(true) : callback(false);
			})
		}
		else
		{
			callback(false)
		}
	});
}




var	commision_referral = function(user_id,amount_coin,callback){
	var coin_balance;
	var new_ast_balance;
	var query;
	var data_update;
	User.findById(user_id, function(err, user_curent) {
		(!err && user_curent) || parseInt(user_curent.p_node) != 0  ? (
			User.findById(user_curent.p_node, function(err, user) {
				!err && user ? (
					coin_balance = parseFloat(user.balance.coin_wallet.available),
					new_ast_balance = parseFloat(coin_balance + amount_coin*0.05),
					query = {_id:user_curent.p_node},
					data_update = {
						$set : {
							'balance.coin_wallet.available': parseFloat(new_ast_balance)
						},
						$push: {
							'balance.coin_wallet.history': {
								'date': Date.now(), 
								'type': 'refferalico', 
								'amount': parseFloat(amount_coin*0.05), 
								'detail': 'Get '+amount_coin*0.05/100000000+' ADA from ID '+user_curent.displayName+' buying '+parseFloat(amount_coin)/100000000+' ADA'
							}
						}
					},
					User.update(query, data_update, function(err, UsersUpdate){
						err ? callback(false) : callback(true);
					})
				) : callback(false)
			})
		): callback(false)
	});
}


function MatchedICO(req, res){
	var query;
	var data_update;
	FindOrder(req.params.id,function(result_order){
		result_order && result_order.status == 0 ? (
			query = {_id:result_order._id},
			data_update = {$set : {'status': 1}},
			Order.update(query,data_update,function(err, newUser){
				update_balance_ico_add(result_order.user_id,parseFloat(result_order.amount_coin),function(cb_add){
					if (cb_add)
					{
						commision_referral(result_order.user_id,parseFloat(result_order.amount_coin),function(callback){
							IcoSum.findOne({}, (err, sum) => { 
								var total = (parseFloat(sum.total) + (parseFloat(result_order.amount_coin)/100000000)).toFixed(8);
						    	sum.total = parseFloat(total);
						        sum.save((err, sum) => {
						        	sendmail_matching_ico(result_order.user_id,function(cbb){
						        		res.redirect('/qweqwe/admin/ico#success')
						        	})
						        });
							});
						})
					}
				})
	        })
		): res.redirect('/qweqwe/admin/ico#error')
	})
}

const sendmail_matching_ico = function (user_id,callback){
	
}

const sendmail_cancel_ico = function (user_id,callback){
	
}

module.exports = {
	ListIco,
	CanelICO,
	MatchedICO,
	ListIcohistory,
	EndICO,
	StartICO,
	TotalBuy
	
}