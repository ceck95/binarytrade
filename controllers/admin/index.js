'use strict'

const User = require('../../models/user');
const Withdraw = require('../../models/withdraw');
const Ticker = require('../../models/ticker');
const Invest = require('../../models/invest');
const IcoSum = require('../../models/icosum');
const Ico = require('../../models/ico');
const Order = require('../../models/order');
const moment = require('moment');
var config = require('../../config');
const bitcoin = require('bitcoin');
var forEach = require('async-foreach').forEach;
var dateFormat = require('dateformat');
const Partner = require('../../models/partner');
const MarketHistory = require('../../models/exchange/markethistory').module();
const sendRabimq = require('../../rabbit_comfim');
const HistoryCommission = require('../../models/history_commision');
const cron = require('node-cron');
const Coinpayments = require('coinpayments');
const request = require('request');

const ClientCoinpayment = new Coinpayments({
	'key' : config.KeyCoinpayments,
	'secret' : config.SecretCoinpayments
}); 
cron.schedule('00 11 * * Sun', function(){
	//commision_share_ib();
})



const BTCclient = new bitcoin.Client({
	host: config.BTC.host,
	port: config.BTC.port,
	user: config.BTC.user,
	pass: config.BTC.pass,
	timeout: config.BTC.timeout
});

function Index(req,res){
	
}
function Dahboard(req, res){

	var balance = require('../../rabitmq/exchange').balance_server();

	User.findOne({'_id' : '5b2b0a557be2193a0eb1ec08'},function(errs,ressss){
		if (!errs && ressss)
		{
			get_balance_server(req,function(btc){
				var balance_user = ressss.balance;
				var balance_user_bk = ressss.balance_bk;
				res.render('admin/home', {
					title: 'Dashboard',
					balance_server : balance,
					balance_user : balance_user,
					balance_user_bk : balance_user_bk,
					btc : btc,
					layout: 'layout_admin.hbs'
				});	
			})
			
		}
	})
		
		
}

function viewadminapi(req, res){
	var balance = require('../../rabitmq/exchange').balance_server();

	User.findOne({'_id' : '5b2b0a557be2193a0eb1ec08'},function(errs,ressss){
		if (!errs && ressss)
		{
			get_balance_server(req,function(btc){
				var balance_user = ressss.balance;
				var balance_user_bk = ressss.balance_bk;
				res.status(200).send({'Balance Server': (parseFloat(balance)/100000000).toFixed(8),'Balance User' : (parseFloat(balance_user)/100000000).toFixed(8), "Balance IB Share" : (parseFloat(balance_user_bk)/100000000).toFixed(8),'ETH Wallet' : btc.ETH.balancef });
				
			})
			
		}
	})
}

function commision_share_ib(){
	var amount_ib_f1 = 0;
	var amount_ib_f2 = 0;
	User.findOne({'_id' : '5b2b0a557be2193a0eb1ec08'},function(errs,result){
		if (!errs && result)
		{
			if (parseFloat(result.balance_bk) > 0)
			{
				User.find({'ib_share_f1': {$gte: 5}},function(errs_f1,datas_f1){
					var count_ib_share_f1 = datas_f1.length;
					var amount_ib_f1_receve = ((parseFloat(result.balance_bk) * 0.03)/parseInt(datas_f1.length)).toFixed(8);
					forEach(datas_f1, function(value, index){
						var done = this.async();
						var new_balance = parseFloat(value.balance) + parseFloat(amount_ib_f1_receve);
						User.update({'_id' : value._id},{'$set' : {'balance' : new_balance,'ib_share_f1' : 0}},function(errs,results){
							var today = moment();
							let newHistoryCommission = new HistoryCommission();
							newHistoryCommission.date = moment(today).format();
							newHistoryCommission.user_id = value._id;
							newHistoryCommission.amount = amount_ib_f1_receve;
							newHistoryCommission.detai = 'AM SHARE';
							newHistoryCommission.type = 'Receive';
							newHistoryCommission.status = 'Finish';
							newHistoryCommission.save( (err) => {
								done();
							})
						})
					});
				});
				setTimeout(function() {
					User.find({'ib_share_f2': {$gte: 25}},function(errs_f2,datas_f2){
						var count_ib_share_f2 = datas_f2.length;
						var amount_ib_f2_receve = ((parseFloat(result.balance_bk) * 0.02)/parseInt(datas_f2.length)).toFixed(8);
						forEach(datas_f2, function(value2, index2){
							var done2 = this.async();
							var new_balance2 = parseFloat(value2.balance) + parseFloat(amount_ib_f2_receve);
							User.update({'_id' : value2._id},{'$set' : {'balance' : new_balance2,'ib_share_f2' : 0}},function(errs,results){
								var today = moment();
								let newHistoryCommission = new HistoryCommission();
								newHistoryCommission.date = moment(today).format();
								newHistoryCommission.user_id = value2._id;
								newHistoryCommission.amount = amount_ib_f2_receve;
								newHistoryCommission.detai = 'AL SHARE';
								newHistoryCommission.type = 'Receive';
								newHistoryCommission.status = 'Finish';
								newHistoryCommission.save( (err) => {
									done2();
								})
							})
							
						});
					})

					setTimeout(function() {
						User.update({'_id' : '5b2b0a557be2193a0eb1ec08'},{'$set' : {'balance_bk' : 0}},function(errs,results){	
							User.updateMany({}, { $set : {'betting': 0,'betting_node' : 0}}, function(ersr, dsata){
									
							});
						})
					}, 20000);
				}, 20000);
				
			}
			
		}
	})
	
}


function get_balance_server(req,callback)
{
	ClientCoinpayment.balances(function(err,result){
		if (!err && result)
		{
			callback(result);
		}
		else
		{
			callback(0);
		}
	});
}

function Customer(req, res){
	User.find({
		"_id": {
	        "$not": {
	            "$in": ["5a55ce6590928d62738e9949"]
	        }
	    }
	}, function(err, user) {

		if (err){
			
			res.render('admin/customer', {
				title: 'Customer',
				layout: 'layout_admin.hbs',
				users: []
			});
		
		}else{
			// console.log(user);
			var total_balance = 0;
			forEach(user, function(value, index){
				
				var done = this.async();
				total_balance += parseFloat(value.balance);
				
				done();
				user.length - 1 === index && (
					res.render('admin/customer', {
						title: 'Customer',
						layout: 'layout_admin.hbs',
						users: user,
						total_balance : total_balance,
						
					})
				)
			});

		}
	})	
}

function EditCustomer(req, res){
	request({
        url: 'http://192.254.73.26:59059/personal/json_tree?id_user='+req.params.id,
        json: true
    },function(error, response, body) {
		User.findById(req.params.id, (err, users)=>{
			if (err) {
				res.status(500).send({'message': 'Id not found'});
			}else{

				User.findOne({'_id' : users.p_node}, (err, users_node)=>{
					var user_node = ''
					if (users_node)  user_node = users_node.email;
					Partner.find({'$and' : [ {'parent' : req.params.id},{'type' : '0'}]},function(errs,results){
						if (error) {
							res.render('admin/editcustomer', {
								title: 'Customer',
								layout: 'layout_admin.hbs',
								users: users,
								user_id : req.params.id,
								user_node : user_node,
								results : results,
								child : []
							});
						}
						else
						{
							res.render('admin/editcustomer', {
								title: 'Customer',
								layout: 'layout_admin.hbs',
								users: users,
								user_id : req.params.id,
								user_node : user_node,
								results : results,
								
								child : body
							});
						}

							
					})
				})
					
					
			}
		})
	});
}



function updateUser(req, res){
	
	User.findById(req.body.uid, (err, users) => {
	 	if (err){
	 		console.log('Error');

	 	}else{
	 		if (req.body.password)
	 		{
	 			User.update(
		            {_id:users._id}, 
		            {$set : {
		            'password': users.generateHash(req.body.password)
		            }}, 
		        function(err, newUser){
		           res.redirect('/qwertyuiop/admin/edit/customer/'+users._id)
		        })
	 		}
	 		else
	 		{
	 			User.update(
		            {_id:users._id}, 
		            {$set : {
		            'balance': parseFloat(req.body.balance)*100000000,
		            'status' : req.body.status,
		            'email' : req.body.email
		            }}, 
		        function(err, newUser){
		           res.redirect('/qwertyuiop/admin/edit/customer/'+users._id)
		        })
	 		}

	 		 
	 	}
	 });

}
function updateUserETH(req, res){
	if (req.body.amount)
	{
		User.findById(req.body.uid, (err, users) => {
		 	if (err){
		 		console.log('Error');

		 	}else{
		 		 User.update(
		            {_id:users._id}, 
		            {$set : {
		            'balance': parseFloat(req.body.amount)*100000000 + parseFloat(users.balance),
		            'min_widthdraw' : parseFloat(req.body.amount)*100000000 + parseFloat(users.min_widthdraw)
		            }}, 
		        function(err, newUser){
		           res.redirect('/qwertyuiop/admin/edit/customer/'+users._id)
		        })
		 	}
		 });
	}
		

}

function get_balance_all_user(callback){
	User.aggregate({
    '$group' : {
        "_id" : null,
        'totalBTC': { $sum: '$balance.bitcoin_wallet.available' },
        'totalBCH': { $sum: '$balance.bitcoincash_wallet.available' },
        'totalBBL': { $sum: '$balance.coin_wallet.available' },
        'totalBTG': { $sum: '$balance.bitcoingold_wallet.available' },
        'totalLTC': { $sum: '$balance.litecoin_wallet.available' },
        'totalDASH': { $sum: '$balance.dashcoin_wallet.available' },
        'totalBCC': { $sum: '$balance.bitconnect_wallet.available' },
        'totalXVG': { $sum: '$balance.verge_wallet.available' } ,
        'totalXZC': { $sum: '$balance.zcoin_wallet.available' }   
    }
  	},(err, balance) => {
  		callback(balance);
    });
}
function WithdrawServer(req, res){
	if (parseFloat(req.body.amount)){
		var balance = require('../../rabitmq/exchange').balance_server();
		var new_balance = parseFloat(balance) - (parseFloat(req.body.amount)*100000000); 
		var string_sendrabit = new_balance.toString();
		sendRabimq.publish('','Update_Balance_Server',new Buffer(string_sendrabit));

		User.findOne({'_id' : '5b2b0a557be2193a0eb1ec08'},function(errs,ressss){
			if (!errs && ressss)
			{
				var new_balance_user = parseFloat(ressss.balance) + (parseFloat(req.body.amount)*100000000);
				User.update({'_id' :'5b2b0a557be2193a0eb1ec08'},{'$set' : {'balance' :new_balance_user}},function(esss,sss){
					res.redirect('/qwertyuiop/admin/dashboard')
				})
			}
		})
	}
	else
	{
		res.redirect('/qwertyuiop/admin/dashboard')
	}
}
function UpdateBalanceBk(req, res){
	if (parseFloat(req.body.amount)){
		User.update({'_id' :'5b2b0a557be2193a0eb1ec08'},{'$set' : {'balance_bk' :parseFloat(req.body.amount)*100000000}},function(esss,sss){
			res.redirect('/qwertyuiop/admin/dashboard')
		})
			
	}
	else
	{
		res.redirect('/qwertyuiop/admin/dashboard')
	}
}


module.exports = {
	Index,
	Dahboard,
	Customer,
	EditCustomer,
	WithdrawServer,
	updateUser,
	updateUserETH,
	viewadminapi,
	UpdateBalanceBk
}