'use strict'

const mongoose = require('mongoose');
const User = require('../models/user');
const Invest = require('../models/invest');
const service = require('../services');
const moment = require('moment');
const nodemailer = require('nodemailer');
const Ticker = require('../models/ticker');
var _ = require('lodash');
const HistoryBalance = require('../models/history_balance');

function TemplateWallStreet(req,res){
	Invest.findOne({user_id: req.user._id},(err,data_invest)=>{
		HistoryBalance.find({user_id: req.user._id},(err,data_history)=>{
			if (!err && data_invest)
			{
				
				res.locals.title = 'Vip ID'
				res.locals.menu = 'wall-street'
				res.locals.user = req.user
				res.locals.amount_wallstreet = data_invest.amount
				res.locals.profit_wallstreet = data_invest.profit
				res.locals.data_wallstreet = data_invest
				res.locals.wallstreet = true
				res.locals.data_history = data_history
				
				res.render('account/wall-street');

			}
			else
			{

				res.locals.title = 'Vip ID'
				res.locals.menu = 'wall-street'
				res.locals.user = req.user
				res.locals.amount_wallstreet = 0
				res.locals.profit_wallstreet = 0
				res.locals.wallstreet = false
				res.locals.data_history = data_history
				res.render('account/wall-street');
			}
		});
	});


			
}

var p_node=[];
var i = 0;
var percent;
var amountInvest;

function InvestSubmit(req,res){
	Invest.findOne({user_id: req.user._id},(err,data_invest)=>{
		if (!data_invest)
		{
			var amount = parseFloat(req.body.amount) * 100000000;
			var user = req.user;
			var balance = parseFloat(user.balance);
			
			if ( amount < 200000000 || isNaN(amount) || amount > 2000000000)
			{
				return res.status(404).send({ message: 'Please enter amount > 2 ETH and amount < 20 ETH'})
			}
			else
			{
				if (parseFloat(amount) > parseFloat(balance))
				{
					return res.status(404).send({ message: 'Ensure wallet has sufficient balance!'})
				}
				else
				{
					var dates = moment();
					let newInvest = new Invest();
					var today = new Date();
					newInvest.date = moment(dates).format();
					newInvest.amount= amount;
					newInvest.profit= parseFloat(amount)*0.20;
					newInvest.withdrawn= 0;
					newInvest.remain= parseFloat(amount)*0.20;
					newInvest.user_id= user._id;
					newInvest.username= req.user.displayName;
					newInvest.status= 1;
					newInvest.status_fundout = 0;
					newInvest.status_widthdraw = 0;
					newInvest.date_finish = today.getTime() + 3600000*24*30;
					newInvest.save((err, investStored)=>{
						if(err)
						{
							return	res.status(500).send({message: `Error invest: ${err}`})	
						}
						else
						{
							var query = {'_id' : user._id};
							var new_balance = (parseFloat(balance) - parseFloat(amount)).toFixed(8);
							var data_update = {
								$set : {
									'balance': parseFloat(new_balance)
								}
							};
							User.update(query, data_update, function(err, Users){
								if(err) res.status(500).send({message: `Error al crear el usuario: ${err}`})
								let newHistoryBalance = new HistoryBalance();
								newHistoryBalance.date = moment(today).format();
								newHistoryBalance.user_id = user._id;
								newHistoryBalance.amount = amount;
								newHistoryBalance.detai = 'Vip ID '+(parseFloat(amount)/100000000).toFixed(8)+' ETH';
								newHistoryBalance.save( (err) => {
									caculateCommission(amount,user, function(callback){
										return res.status(200).send({message: 'Subscribe success'}) 
									})	
								})
							});
						}
					})
				}
			}
		}
		else
		{
			return res.status(404).send({ message: 'Error'})
		}
	})		
}

function InvestSubmitWidthdraw(req,res){
	Invest.findOne({user_id: req.user._id},(err,data_invest)=>{
		if (data_invest)
		{	

			if (parseInt(data_invest.status_fundout) == 1)
			{
				var amount = parseFloat(req.body.amount) * 100000000;
				if ( amount < 0 || isNaN(amount))
				{
					return res.status(404).send({ message: 'Please enter amount ETH'})
				}
				else
				{
					var user = req.user;
					var balance = parseFloat(data_invest.remain) * 0.5;
					
					
					if (parseFloat(amount) > parseFloat(balance))
					{
						return res.status(404).send({ message: 'The number you withdraw is greater than the allowable system!'})
					}
					else
					{
						var query = {'user_id' : user._id};
						var new_remain = (parseFloat(balance) - parseFloat(amount)).toFixed(8);
						var new_widthdraw = (parseFloat(data_invest.withdrawn) + parseFloat(amount)).toFixed(8);
						var data_update = {
							$set : {
								'remain': parseFloat(new_remain),
								'withdrawn': parseFloat(new_widthdraw)
							}
						};
						Invest.update(query, data_update, function(err, Users){
							if(err) res.status(500).send({message: `Error al crear el usuario: ${err}`})
							
							var querys = {'_id' : user._id};
							var new_balances = (parseFloat(user.balance) + parseFloat(amount)).toFixed(8);
							var data_updates = {
								$set : {
									'balance': parseFloat(new_balances)
								}
							};
							User.update(querys, data_updates, function(err, Users){
								let newHistoryBalance = new HistoryBalance();
								var today = new Date();
								newHistoryBalance.date = moment(today).format();
								newHistoryBalance.user_id = user._id;
								newHistoryBalance.amount = amount;
								newHistoryBalance.detai = 'Fund Out '+(parseFloat(amount)/100000000).toFixed(8)+' ETH';
								newHistoryBalance.save( (err) => {
									return res.status(200).send({message: 'Subscribe success'}) 
								})
							})
						});
					}
				}
			}
			else
			{
				return res.status(404).send({ message: 'Error'})
			}
			
		}
		else
		{
			return res.status(404).send({ message: 'Error'})
		}
	})		
}

function caculateCommission(amountInvest,users, callback) {
    User.findOne({'_id':users.p_node}, function (err, user) {
    	
		if (err || !user)
		{
			return callback(err);
		}
		else
		{
			var commission = parseFloat(amountInvest)*0.03;
			var new_balance = parseFloat(user.balance) + commission;
			User.update({'_id':users.p_node}, {'$set' : {'balance' : new_balance}}, function(err, Users){
				let newHistoryBalance = new HistoryBalance();
				var today = new Date();
				newHistoryBalance.date = moment(today).format();
				newHistoryBalance.user_id = users.p_node;
				newHistoryBalance.amount = commission;
				newHistoryBalance.detai = 'Referral commission '+(parseFloat(commission)/100000000).toFixed(8)+' ETH from F1 '+users.email;
				newHistoryBalance.save( (err) => {
					
					return callback(commission);
				})
			});
		} 
    });
}


module.exports = {
	TemplateWallStreet,
	InvestSubmit,
	InvestSubmitWidthdraw
}