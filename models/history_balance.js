'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const HistoryBalancechema = new Schema({
	user_id : String,
	amount : String,
	date: { type: Date, default: Date.now() },
	detai : String
});



var HistoryBalance = mongoose.model('HistoryBalance', HistoryBalancechema);
module.exports = HistoryBalance;