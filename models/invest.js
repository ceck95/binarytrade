'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Investchema = new Schema({
	date: { type: Date, default: Date.now() },
    date_finish: { type: Date},
	amount: String,
    withdrawn: String,
	profit: String,
	remain: String,
	user_id: String,
    status: String,
    username: String,
    status_fundout : String,
    status_widthdraw : String
});



var Invest = mongoose.model('Invest', Investchema);
module.exports = Invest;